import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.css';
import '../index.css';
function ListadoCursos(){
   
    const [listCursos,setListCursos]=useState([]);
    //forma de hacerlo
   /* useEffect(function () {
        fetch("http://localhost:12345/cursos").then((resp)=>
        resp.json()).then((json)=>
        {   
            setListCursos(json.cursos);
        }
             
        
        );
        console.log(listCursos);
    },[])*/
    useEffect(() => {
        const getData = async () => {
          const url = "http://localhost:1234/cursos";
      
          try {
            const resp = await fetch (url);
            const data = await resp.json();
            
            setListCursos(data.cursos);
          } catch (err) {
            console.error(err);
          }
        }
      
        getData();
      }, []);

    return(
        <Table striped bordered hover variant="dark" className="w-3/12 h-1/2 max-h-max col-auto max-w-min"> 
            
            <thead>
         
    <tr>
        <th>
            curso
        </th>
        <th>
            horas
        </th>
        
         </tr>
            </thead>
            <tbody>
            {listCursos.map((cursos)=>(
                <tr>
                <td>
                    {cursos.materia}
                </td>
                <td>
                    {cursos.horas}
                </td>
                </tr>   ))}
                
            </tbody>



        </Table>


    )

}

export default ListadoCursos;
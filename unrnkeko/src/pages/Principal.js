import '../App.css'
import Estudiante from '../Estudiante';
import React, { useEffect, useRef, useState } from 'react';
import { render } from '@testing-library/react';
import { Button, Dropdown, DropdownButton, Form } from 'react-bootstrap';
import Cursos from './Cursos';
import { useParams } from 'react-router-dom';


function Principal() {
   //useParams(usuario);
    const curse=useRef();
    const[listCursos,setListCursos]=useState([]);
    const[estudiante,setEstudiante]=useState([{}]);
    const [curso,setCurso]=useState({});
   
    const [curses,setCurses]=useState();


   // const { usuario } = this.props.location
   
    function handleSubmit(e) {
        e.preventDefault()
        //const {nombre,apellido } = e.target.elements;
     
       
        fetch("http://localhost:1234/estudiantes",{
          method:"POST",
          body:JSON.stringify(
            { nombre:curses.nombre,
              apellido:curses.apellido,
              id:curso.id,
              materia:curso.materia,
              horas:curso.horas,
              legajo:curses.legajo

            }
          ),

        }).then((resp)=>resp.json).then((json)=>{
          if(json.result==="error"){
            console.log("ah ocurrido un error");
            return
          }

          console.log("ah cargado con exito");
        })
       
        

        
        
    }
  
    useEffect(function () {
      fetch("http://localhost:1234/cursos").then((resp)=>
      resp.json()).then((json)=>
      {   
          setListCursos(json.cursos);
          console.log(listCursos)
        //  console.log("el usuario es:"+usuario);
        const usuarioStorage=localStorage.getItem('nombre');
        const items = JSON.parse(localStorage.getItem("usuario"));
        console.log("el nombre storage: "+items.data.usuario);
        console.log("el usuario en el storage es: " +localStorage.getItem("usuario").data);
      }
           
      
      );
      
  },[])

     
      
      function handleSelect(e){
        e.preventDefault();
        let cursitoId=e.target.value;
        console.log(cursitoId)
        listCursos.forEach(element => {
          if(cursitoId==element.id){
            setCurso(element);
            console.log(curso);
          }
        });
       
        
        
    }
    const obtenerInput=(e)=>{

      setCurses({...curses,[e.target.name]:e.target.value});
      console.log(curses);
    }


  return (  
    <div className=" block items-center px-4 py-2 rounded-md space-x-3 w-96 content-center object-left  ">
  
   
     

   
  


   
          
          <Form onSubmit={handleSubmit} className="mt-44">
          <Form.Group >
          <Form.Label>nombre</Form.Label>
       <Form.Control type="text" placeholder="nombre" name="nombre" onChange={obtenerInput}></Form.Control>
       </Form.Group>
       <Form.Group >
          <Form.Label>apellido</Form.Label>
       <Form.Control type="text" placeholder="apellido" name="apellido" onChange={obtenerInput}></Form.Control>
       </Form.Group>
       <Form.Group>
       <Form.Label>cursos</Form.Label>
       <Form.Control 
        name="curso"
        onChange={handleSelect}
        as="select" >
         {listCursos.map((x)=>(<option value={x.id}>{x.materia}</option>))}
          
       
       </Form.Control>
       </Form.Group>
        <p>{curso.materia}</p>
        <Form.Group >
        <Form.Label>legajo</Form.Label>
          <Form.Control type="text" placeholder="legajo" name='legajo' onChange={obtenerInput}></Form.Control>
       </Form.Group>
       <Button as="input" type="submit" value="Login" />
       
      
    
    </Form>
      
      </div>
  );
  //<Estudiante estudiante={estudiante}cursos={curse.current}/>
}

export default Principal;
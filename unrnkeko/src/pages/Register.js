import { Link } from "react-router-dom";
import {useEffect, useState} from "react";
import { useForm } from "react-hook-form";
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import NavBar from "../components/NavBar";
import Principal from "./Principal";

function Register(){

    const { register, handleSubmit } = useForm();
   let  [authMode, setAuthMode] = useState("signin");
  const [usuario,setUsuario]=useState({nombre:'',email:'',password:'',isLogin:false});
  
  const [estaLogeado,setLogeado]=useState({
    cartelLogeado:"no se logeo",
    
    boleanoLogged:false});
 
  const changeAuthMode = () => {
    setAuthMode(authMode === "signin" ? "signup" : "signin")
  }

  const onSubmit = data => {
 
    //const {nombre,apellido } = e.target.elements;
    console.log(data.nombre);
    console.log(data.email);
    console.log(data.password);
    fetch("http://localhost:1234/usuarios",{
      method:"POST",
      body:JSON.stringify(
        { nombre:data.nombre,
          email:data.email,
          password:data.password,
         

        }
      ),

    }).then((resp)=>resp.json).then((json)=>{
      if(json.result==="error"){
        console.log("ah ocurrido un error");
        return
      }

      
    })


  }
 
  
 
  function isEmptyObject(obj){
    
    if(obj!=null){
      console.log("no es null")
      return true;
    }
    console.log("es null")
    return false;
}
  const ingresarUsuario = data => {
 
    console.log(data.email);
    console.log(data.password);
   
   
    fetch("http://localhost:1234/usuario/"+data.email+","+data.password).then((resp)=>
    resp.json()).then((json)=>
    { 
      if(isEmptyObject(json.usuario)===true){
        console.log("entro aca tiene cuenta el nombre es: "+json.usuario.nombre)
        setLogeado({cartelLogeado:"se encuentra en el sistema",boleanoLogged:true});
        setUsuario(json.usuario);
        
        localStorage.setItem("usuario", JSON.stringify(json.usuario));
        
       
        console.log(estaLogeado.boleanoLogged);
        
      }
     
      if(isEmptyObject(json.usuario)===false){
        setLogeado({cartelLogeado:"no se encuentra en el sistema",boleanoLogged:false});
      }
     
      console.log(json)
   
    }

         
   
    ).catch((e)=>{
      console.log(e)
    });}
   

    useEffect(() => {
     let usuario= JSON.parse(localStorage.getItem("usuario"));
     if(usuario!=null){

     
      setUsuario({usuario,isLogin:true});}
      
    },[]);
  
  

    if(usuario.isLogin===true){
      return(
      <div>
      
        <h1>{usuario.usuario.nombre}</h1>
      </div>
      )
    }



  if (authMode === "signin") {
    return (

      <div className="Auth-form-container">
       
        <form className="Auth-form" onSubmit={handleSubmit(ingresarUsuario)}>
          <div className="Auth-form-content">
            <h3 className="Auth-form-title">Sign In</h3>
            <div className="text-center">
              Not registered yet?{" "}
              <span className="link-primary" onClick={changeAuthMode}>
                Sign Up
              </span>
            </div>
            <div className="form-group mt-3">
              <label>Email address</label>
              <input
                type="email"
                className="form-control mt-1"
                placeholder="Enter email"
                {...register("email")}
              />
            </div>
            <div className="form-group mt-3">
              <label>Password</label>
              <input
                type="password"
                className="form-control mt-1"
                placeholder="Enter password"
                {...register("password")}
              />
            </div>
            <div className="d-grid gap-2 mt-3">
              
              <button 
             
              type="submit" className="btn btn-primary">
                Submit
              </button>
              {estaLogeado.boleanoLogged ? (
           <Link to={{ pathname: "/Principal", state: usuario }} />

         ) : null}
            </div>
            <p className="text-center mt-2">
              Forgot <a href="#">password?</a>
            </p>
          </div>
        
        </form>
        <p >{estaLogeado.boleanoLogged?usuario.nombre: estaLogeado.cartelLogeado}</p>
       
      </div>
 
    )
  }


  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleSubmit(onSubmit)}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="text-center">
            Already registered?{" "}
            <span className="link-primary" onClick={changeAuthMode}>
              Sign In
            </span>
          </div>
          <div className="form-group mt-3">
            <label>Full Name</label>
            <input
              type="text"
              required
              className="form-control mt-1"
              placeholder="e.g Jane Doe"
              {...register("nombre")}
            />
          </div>
          <div className="form-group mt-3">
            <label>Email address</label>
            <input
              type="email"
              className="form-control mt-1"
              placeholder="Email Address"
              {...register("email")}
            />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              className="form-control mt-1"
              placeholder="Password"
              {...register("password")}
            />
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
          <p className="text-center mt-2">
            Forgot <a href="#">password?</a>
          </p>
        </div>
      </form>
    </div>
  )
}
export default Register;
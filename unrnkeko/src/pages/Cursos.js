import { Button, Dropdown, DropdownButton, Table } from "react-bootstrap";
import { useEffect, useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import ListadoCursos from "../components/ListadoCursos";
import 'bootstrap/dist/css/bootstrap.css';
import index from "../index.css"
function Cursos() {
    
    const [estudiantes,setEstudiantes]=useState([]);
    const[cursos,setCursos]=useState([]);
   const[listCursos,setListCursos]=useState([]);
   
    
    const listarEstudiantes=async()=>{
        setEstudiantes([]);
        fetch("http://localhost:1234/estudiantes").then((resp)=>
        resp.json()).then((json)=>
        {   if(estudiantes.length===0){
            setEstudiantes(...estudiantes,json.estudiantes);
        }
             
        
       // data=Array.from(json.estudiantes);
        });
        
  
        
    }
    
   
   useEffect(() => {
        const getData = async () => {
          const url = "http://localhost:1234/cursos";
      
          try {
            const resp = await fetch (url);
            const data = await resp.json();
            
            setListCursos(data.cursos);
          } catch (err) {
            console.error(err);
          }
        }
      
        getData();
      }, []);
    
   
    /*const listMateriasEstudiante=async()=>{
    
        
        fetch("http://localhost:1234/cursos").then((resp)=>
        resp.json()).then((json)=>
        {   if(estudiantes.length===0){
            setListCursos(json.cursos);
        }
             
        
       // data=Array.from(json.estudiantes);
        });
        
  
        
    }*/
    const cursosEstudiante=async(legajo)=>{
        
        fetch("http://localhost:1234/estudiantes").then((resp)=>
        resp.json()).then((json)=>
        {   
            
            json.estudiantes.forEach((est,index) => {
                
                if(est.legajo===legajo){
                    
                        
                    setCursos(
                        json.estudiantes[index].cursos);
                        
                }
               
                
            });
            
        
    })}
    
 

    return (
<div className="flex items-start mt-52 float-left px-4 py-2 rounded-md space-x-3 w-96 content-center  "><Button onClick={listarEstudiantes} variant="dark">listar estudiantes</Button>

        <Table striped bordered hover variant="dark" className="w-3/12 h-1/2 max-h-max columns-1 max-w-min tabla">
            
  <thead>
    <tr>
        <th>id</th>
      <th>nombre</th>
      <th>apellido</th>
      <th>legajo</th>
      <th>cursos</th>
      <th>materias</th>
      
      
      
    </tr>
  </thead>
  <tbody >
  
  {estudiantes.map((est)=>(
    <tr>
        <td key={est.id}>{est.id}</td>
        <td >{est.nombre}</td>
        <td >{est.apellido}</td>
        <td >{est.legajo}</td>
        <td ><Button variant="success" onClick={()=>cursosEstudiante(est.legajo)}>listar materias del estudiante</Button></td>
        <td >
            
            <DropdownButton  id="dropdown-basic-button" title="agregar materias">
            {listCursos.map((x,c) => (<Dropdown.Item>{x.materia}</Dropdown.Item>))}
            </DropdownButton>
            </td>
        </tr>))}</tbody> 
        </Table>
        <Table striped bordered hover variant="dark" className="tabla columns-1 ">
        <thead>
        <tr>
        <th>materia</th>
        <th>horas</th>
        </tr>
        </thead>
        <tbody>
       {cursos.map((cur)=>(
    <tr>  
        
        <td>{cur.materia}</td>
        <td>{cur.horas}</td>
  
        </tr>))}</tbody>
        </Table>
    
    <ListadoCursos />
    </div>


    );
    
}



export default Cursos;
import {
    BrowserRouter,
    Routes,
    Route
  } from "react-router-dom";
import NavBar from "../components/NavBar";
import Cursos from "../pages/Cursos";
import Principal from "../pages/Principal";
import Register from "../pages/Register";
import PaginaTailwind from "../pages/PaginaTailwind";

function Routeo() {
    return (
<div>
     
<BrowserRouter>
         <NavBar/>
        <Routes>
        <Route  path="/cursos" element={<Cursos/>}>
                
            </Route>
            <Route  path="/principal" element={<Principal/>}>
                
            </Route>
            <Route  path="/register" element={<Register/>}>
                
                
                </Route>
            <Route  path="/tailwind" element={<PaginaTailwind/>}>
                
                </Route>
           
           
            </Routes>
         
          
            
            
    


        </BrowserRouter>
    </div>)
}
    export default Routeo;